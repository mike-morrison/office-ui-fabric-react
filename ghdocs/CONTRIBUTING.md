# Contribution

Right now, the project is in a **pre-v1** state, so we're mainly driving torwards the goals outlined in the [roadmap](https://github.com/OfficeDev/office-ui-fabric-react/blob/master/ghdocs/ROADMAP.md). With this in mind, feel free to file bugs but new components or additional features will probably not be considered to be executed on until after the v1 state is reached.

If you have a bug fix for which you'd like to submit a pull request, please ensure that you sign the CLA via the CLA bot's instructions and engineers from the team will review it asap.
